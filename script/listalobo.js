let generatedNumbers = new Set();

function generateRandom() {
    let randomNumber;
    do {
        randomNumber = Math.floor(Math.random() * 1000) + 1;
    } while (generatedNumbers.has(randomNumber)); 
    generatedNumbers.add(randomNumber); 
    return randomNumber;
}
function createWolfRow(name, age, description, image, data) {
    let wolvesRow = document.createElement("div");
    wolvesRow.classList.add("wolves-display");

    
    let linkElement = document.createElement('a');
    
    linkElement.addEventListener("click", () => {
        localStorage.setItem("selectedWolf", JSON.stringify(data));
        window.location.href = "/showlobinho.html";
    });
    
    let wolfImg = document.createElement("img");
    wolfImg.src = image;
    let wolfImgDiv = document.createElement("div");
    wolfImgDiv.append(wolfImg);
    wolfImgDiv.classList.add("wolf-img-div");
    wolfImgDiv.id = "wolf-reverse-img-shadow";
    wolvesRow.append(wolfImgDiv);;

    let wolfAdop = document.createElement("div")
    wolfAdop.classList.add("divBtn")

    let adopBtn = document.createElement("button");
    let adopText = document.createElement("h1");
    adopBtn.classList.add("btnAdop");
    if(data.adotado==true){
        adopText.innerText = "Adotado";
    }
    else{
        adopText.innerText = "Adotar";
        adopBtn.addEventListener('click',()=>{
            window.location.href = "/adicionarlobinho.html";
        })
    }
    adopText.classList.add("htext");
    adopBtn.append(adopText);
    wolfAdop.append(adopBtn);

    let wolfText = document.createElement("div");
    wolfText.classList.add("wolves-texto");
    let wolfName = document.createElement("h2");
    wolfName.innerText = name;
    let wolfAge = document.createElement("span");
    wolfAge.innerText = ("Idade: " + age);
    let wolfDscription = document.createElement("p");
    wolfDscription.innerText = description;
    wolfText.append(wolfName);
    wolfText.append(wolfAge);
    wolfText.append(wolfDscription);
    wolvesRow.append(wolfText);
    wolvesRow.append(adopBtn)
    return wolvesRow;
}


function adopted(event) {
    let button = event.target;
    button.innerText = "Adotado";
    button.classList.add("clicked");
    button.removeEventListener("click", adopted);
};



function setWolfRow(name, age, description, image, data) {
    wolvesRow = createWolfRow(name, age, description, image, data);
    let wolvesSection = document.getElementById("wolves-section");
    wolvesSection.append(wolvesRow);
}
function setReverseWolfRow(name, age, description, image, data) {
    wolvesRow = createWolfRow(name, age, description, image, data);
    wolvesRow.id = "wolves-display-reverse"
    let wolvesSection = document.getElementById("wolves-section");
    wolvesSection.append(wolvesRow);
}

async function getWolfCard(pageNumber){
    try{
    const response = await fetch('../api/lobinhos.json'); 
    const data = await response.json();

    const itemsPerPage = 4; // Defina o número de lobos por página
    const startIndex = (pageNumber - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;

    const wolfList = document.getElementById("wolves-section");
    wolfList.innerHTML = "";

    for(let i = startIndex; i < endIndex && i < data.length; i++){ 
        if(i%2!=0){
            setReverseWolfRow(data[i].nome, data[i].idade, data[i].descricao, data[i].imagem, data[i])
        }
        else{
            setWolfRow(data[i].nome, data[i].idade, data[i].descricao, data[i].imagem, data[i]); 
        }
    }
    
    renderPaginationButtons(pageNumber, Math.ceil(data.length / itemsPerPage));
    }catch(err){
        console.error(err)
    }    
}

document.addEventListener("DOMContentLoaded", () => {
    getWolfCard(1);
});

function renderPaginationButtons(currentPage, totalPages) {
    const paginationContainer = document.getElementById("pagination");
    paginationContainer.innerHTML = "";

    const previousButton = document.createElement("button");
    previousButton.textContent = "Anterior";
    previousButton.disabled = currentPage === 1;
    previousButton.addEventListener("click", () => {
        getWolfCard(currentPage - 1);
    });

    const nextButton = document.createElement("button");
    nextButton.textContent = "Próximo";
    nextButton.disabled = currentPage === totalPages;
    nextButton.addEventListener("click", () => {
        getWolfCard(currentPage + 1);
    });

    paginationContainer.appendChild(previousButton);
    paginationContainer.appendChild(nextButton);
}

function search() {
    let secao = document.querySelector("#wolves-section");
    secao.innerHTML = "";
    let input = document.querySelector(".searchInput").value;
    const fetchConfig = {
        "method": "GET",
    };
    fetch("../api/lobinhos.json", fetchConfig)
        .then((response) => {
            response.json()
                .then((response) => {
                    let data = response.find((wolf) => {
                        if (isNaN(input)) {
                            
                            return wolf.nome.toLowerCase().includes(input.toLowerCase());
                        } else {
                            
                            return wolf.id === parseInt(input);
                        }
                    });
                    if (data) {
                        setWolfRow(data.nome, data.idade, data.descricao, data.imagem, data);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        })
        .catch((error) => {
            console.log(error);
        });
    document.querySelector(".searchInput").value = "";
}




let searchBtn = document.querySelector(".btnSearch")
searchBtn.addEventListener("click", ()=>{search()})





