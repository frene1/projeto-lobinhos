var url = window.location.href.split("wolves/");
var id_lobo =  url [1];

const fetchConfig = {
    "method" : "GET"
}
fetch (`${/api/lobinhos.json}/${id_lobo}` , fetchConfig)
    .then((resposta)=>{
        resposta.json()
        .then((resposta)=>{
                let colocar_nome = document.querySelector(".nome_lobo");
                let colocar_img = document.querySelector(".img_lobo");
                let colocar_id = document.querySelector(".id_lobo");
                colocar_nome.innerText = "Adote o " + resposta.nome;
                colocar_img.src = resposta.imagem;
                colocar_id.innerText = "#" + resposta.id;
            })
        .catch()
    })
    .catch() 


function receber_msg (){
    let input_nome = document.querySelector(".digitar_nome").value;
    let input_email = document.querySelector(".digitar_email").value;
    let input_idade = document.querySelector(".digitar_idade").value;
    let fetchbody = {
        "adotado" : true,
        "nomeDono" : input_nome,
        "emailDono" : input_email,
        "idadeDono" : input_idade
    }
    const fetchConfig1 = {
        "method" : "PUT",
        "body" : JSON.stringify(fetchbody),
        "headers" : {"Content-Type" : "application/json"}
    }
    fetch (`${/api/lobinhos.json}/${id_lobo}` , fetchConfig1);
}

let btn_enviar = document.querySelector("#enviar_msg");
btn_enviar.addEventListener("click", receber_msg);
